#include <ros/ros.h>
#include <dji_sdk/RCChannels.h>
#include <dji_sdk/FlightControlInfo.h>
#include <cmath>
#include "mbzirc_mission/mission_modes.h"
#include "blink1/Blink1.h"
#include <message_filters/subscriber.h>


int firstTime=1;
double prevButton;

class ffcontrol
{
public:

    Blink1* led;
    ros::NodeHandle n_;
    void targetCallback(const dji_sdk::RCChannels::ConstPtr &msg);
    void blink_binary(double);
    void blink(bool b);
};

void ffcontrol::blink_binary(double val)
{
    int ival = round((double)(val*10.0 - 40.0));
    bool b3,b2,b1;
    b1 = ival & (1<<0);
    b2 = ival & (1<<1);
    b3 = ival & (1<<2);
    ROS_INFO_STREAM("\n"<<val<<":"<<ival<<":"<<b3<<b2<<b1);
    ROS_INFO("%.2f:%d:%d%d%d",val,ival,b3,b2,b1);

    blink(b3);
    blink(b2);
    blink(b1);
    led->set(0, 0, 0);
    ros::Duration(0.2).sleep();
}

void ffcontrol::blink(bool b)
{
    if(b)
        led->set(0, 255, 0);
    else
        led->set(255, 0, 0);

    ros::Duration(0.2).sleep();
    led->set(0, 0, 0);
}

void ffcontrol::targetCallback(const dji_sdk::RCChannels::ConstPtr &msg){
    double ff15;
    if(firstTime==1){
        prevButton=msg->gear;
        firstTime = 0;
    } else{
        if(prevButton!=msg->gear){
            if(msg->mode < -1.0){ //if mode P -- manual)
                n_.getParamCached("/mission_control/ff15", ff15);
                ff15=ff15+0.1f;

//                n_.setParam("/mission_control/ff15",ff15);
//                prevButton=msg->gear;
//                blink_binary(ff15);
            }
            if(msg->mode == 0.0){
                n_.getParamCached("/mission_control/ff15", ff15);
                ff15=ff15-0.1f;

//                n_.setParam("/mission_control/ff15",ff15);
//                prevButton=msg->gear;
//                blink_binary(ff15);
            }
//            ff15 = (ff15 > 4.7f) ? 4.7f : ff15;
            ff15 = (ff15 < 4.0f) ? 4.0f : ff15;
            n_.setParam("/mission_control/ff15",ff15);
            prevButton=msg->gear;
            blink_binary(ff15);
        }
    }

}  

int main(int argc, char** argv){
  ros::init(argc, argv, "ffcontrol");

  ros::NodeHandle n;
  ffcontrol ffobj;
  ros::Subscriber sub1 = n.subscribe("/dji_sdk/rc_channels", 3 ,&ffcontrol::targetCallback, &ffobj);

  bool hasBlink = true;
//  n.param("/visual_servoing/hasBlink", hasBlink, true);

  if (hasBlink) ffobj.led = new Blink1(n);
  ros::Duration(2.0).sleep();
  if (hasBlink) ffobj.led->blink(0, 0, 255, 300); // Blinking Blue
  
  ros::Rate loop_rate(4);
  ros::Duration(4.0).sleep(); 
  double ff15;
  n.param("/mission_control/ff15", ff15, 4.2);
  n.getParamCached("/mission_control/ff15", ff15);

  while (ros::ok())
  {
        ros::spinOnce();
        loop_rate.sleep();
  }

  return 0;
}
